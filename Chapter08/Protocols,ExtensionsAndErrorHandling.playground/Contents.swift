import UIKit

// protocols
// create protocol
protocol CalorieCountProtocol {
     var calories: Int { get }
     func description() -> String
}

// use protocol
class Burger : CalorieCountProtocol {
     let calories = 800
     func description() -> String {
          return "This burger has \(calories) calories"
     }
}
struct Fries : CalorieCountProtocol {
     let calories = 500
     func description() -> String {
          return "These fries have \(calories) calories"
     }
}

// extensions
// Adopting a protocol via an extension
enum Sauce {
     case chili
     case tomato
}
extension Sauce : CalorieCountProtocol {
     var calories : Int {
          switch self {
               case .chili:
                    return 20
               case .tomato:
                    return 15
          }
     }
     func description() -> String {
          return "This sauce has \(calories) calories"
     }
}

// Creating an array of different types of objects
let burger = Burger()
let fries = Fries()
let sauce = Sauce.tomato
let foodArray : [CalorieCountProtocol] = [burger, fries,  sauce]
var totalCalories = 0
for food in foodArray {
     totalCalories += food.calories
}
print(totalCalories)

// error handling
enum WebpageError: Error {
     case success
     case failure(Int)
}
func getWebpage(uRL: String, siteUp: Bool) throws ->   String {
     if siteUp == false {
          throw WebpageError.failure(404)
     }
     return "Success"
}
let webpageURL = "http://www.apple.com"
let websiteUp = true
try getWebpage(uRL: webpageURL, siteUp: websiteUp)

do {
     let status = try getWebpage(uRL: webpageURL, siteUp:
     websiteUp)
     print(status)
} catch {
     print(error)
}

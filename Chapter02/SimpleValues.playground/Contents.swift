import UIKit

// SimpleValues
42
-23
3.14159
0.1
-273.15
true
false
"hello, world"
"albatross"

// constants and variables
let theAnswerToTheUltimateQuestion = 42
let pi = 3.14159
let myName = "Ahmad Sahar"

var currentTemperatureInCelsius = 27
var myAge = 50
var myLocation = "home"

let isRaining = true
// next line will not compile, because constant cannot be changed
// isRaining = false

var isNotRaining = true
// this will compile, becuase variable can be changed
isNotRaining = false

// type inference and type safety
let cuisine = "American"

var restaurantRating: Double = 3 // use type annotation to declare variable's type
// next line will not compile, because cannot change type of variable
// restaurantRating = "Good"

// operators
let sum = 23 + 20
let result = 32 - sum
let total = result * 5
let divide = total/10

let a = 12
let b = 12.0
// next line will not compile, because cannot use operators on different types
// let c = a + b
// this is how to fix previous issue
let c = Double(a) + b

// compound assignment operators
var aa = 1
aa += 2
aa -= 1

// comparison operators
1 == 1
2 != 1
2 > 1
1 < 2
1 >= 1
2 <= 1

// logical operators
(1 == 1) && (2 == 2)
(1 == 1) && (2 != 2)
(1 == 1) || (2 == 2)
(1 == 1) || (2 != 2)
(1 != 1) || (2 != 2)
!(1 == 1)

// string operations
let greeting = "Good" + " Morning"

let rating = 3.5
var ratingResult = "The restaurant rating is " +
String(rating)

// string interpolation
ratingResult = "The restaurant rating is \(rating)"

// print() instruction
print(ratingResult)

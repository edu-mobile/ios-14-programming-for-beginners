import UIKit

// Classes
// a class declaration
class Animal {
     var name: String = ""
     var sound: String = ""
     var numberOfLegs: Int = 0
     var breathesOxygen: Bool = true
     func makeSound() {
          print(self.sound)
     }
}

// class instance creation
let animal = Animal()
print(animal.name)
print(animal.sound)
print(animal.numberOfLegs)
print(animal.breathesOxygen)
animal.makeSound()

let cat = Animal()
cat.name = "Cat"
cat.sound = "Mew"
cat.numberOfLegs = 4
cat.breathesOxygen = true
print(cat.name)
print(cat.sound)
print(cat.numberOfLegs)
print(cat.breathesOxygen)
cat.makeSound()
print()

class Animal2 {
     var name: String
     var sound: String
     var numberOfLegs: Int
     var breathesOxygen: Bool
     init(name: String, sound: String, numberOfLegs: Int,       breathesOxygen: Bool) {
          self.name = name
          self.sound = sound
          self.numberOfLegs = numberOfLegs
          self.breathesOxygen = breathesOxygen
     }
     func makeSound() {
          print(self.sound)
     }
}

let cat2 = Animal2(name: "Cat", sound: "Mew", numberOfLegs: 4, breathesOxygen: true)
print(cat2.name)
print(cat2.sound)
print(cat2.numberOfLegs)
print(cat2.breathesOxygen)
cat2.makeSound()
print()

// making a subclass
class Mammal: Animal2 {
     let hasFurOrHair: Bool = true
}

let cat3 = Mammal(name: "Cat", sound: "Mew", numberOfLegs:  4, breathesOxygen: true)
print(cat3.hasFurOrHair)

// Overriding a superclass method
class Animal3 {
     var name: String
     var sound: String
     var numberOfLegs: Int
     var breathesOxygen: Bool = true
     init(name: String, sound: String, numberOfLegs: Int,       breathesOxygen: Bool) {
          self.name = name
          self.sound = sound
          self.numberOfLegs = numberOfLegs
          self.breathesOxygen = breathesOxygen
     }
     func makeSound() {
          print(self.sound)
     }
     func description() -> String{
     return "name: \(self.name) sound: \(self.sound)     numberOfLegs: \(self.numberOfLegs) breathesOxygen:      \(self.breathesOxygen)"
     }
}
class Mammal2: Animal3 {
     let hasFurOrHair: Bool = true
}

let cat4 = Mammal2(name: "Cat", sound: "Mew", numberOfLegs:  4, breathesOxygen: true)
print(cat4.description())
cat4.makeSound()

class Mammal3: Animal3 {
     let hasFurOrHair: Bool = true
     override func description() -> String {
          return super.description() + " hasFurOrHair: \(self.hasFurOrHair)"
     }
}
let cat5 = Mammal3(name: "Cat", sound: "Mew", numberOfLegs:  4, breathesOxygen: true)
print(cat5.description())
cat5.makeSound()
print()

// structure
// Creating a structure declaration
struct Reptile {
     var name: String
     var sound: String
     var numberOfLegs: Int
     var breathesOxygen: Bool
     let hasFurOrHair: Bool = false
     func makeSound() {
          print(sound)
     }
     func description() -> String {
          return "Structure: Reptile name: \(self.name)           sound: \(self.sound) numberOfLegs:           \(self.numberOfLegs) breathesOxygen:           \(self.breathesOxygen) hasFurOrHair:           \(self.hasFurOrHair)"
     }
}

// Making an instance of the structure
var snake = Reptile(name: "Snake", sound: "Hiss",   numberOfLegs: 0, breathesOxygen: true)
print(snake.description())
snake.makeSound()

// Comparing value types and references types
// struct is value type and when copied, new copy is created and old copy isn't affected by changes to the new
struct SampleValueType {
    var sampleProperty = 10
}
var a = SampleValueType()
var b = a
b.sampleProperty = 20
print(a.sampleProperty)
print(b.sampleProperty)

// class is a reference type and when copied, memory reference copy is created, any changes to any of the reference type copies will affect both
class SampleReferenceType {
     var sampleProperty = 10
}
var c = SampleReferenceType()
var d = c
c.sampleProperty = 20
print(c.sampleProperty)
print(d.sampleProperty)
print()

// enumerations
// Creating an enumeration
enum TrafficLight {
    case red
    case yellow
    case green
    func trafficLightDescription() -> String {
              switch self {
              case .red:
                   return "red"
              case .yellow:
                   return "yellow"
              default:
                   return "green"
              }
         }
}
var trafficLight = TrafficLight.red
print(trafficLight.trafficLightDescription())


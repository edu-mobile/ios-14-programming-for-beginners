//
//  RestaurantCell.swift
//  LetsEat
//
//  Created by Eduard Luhtonen on 31.07.22.
//

import UIKit

class RestaurantCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCuisine: UILabel!
    @IBOutlet weak var imgRestaurant: UIImageView!
}

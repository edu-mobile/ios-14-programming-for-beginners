//
//  Segue.swift
//  LetsEat
//
//  Created by Eduard Luhtonen on 30.07.22.
//

import Foundation

enum Segue: String {
    case showDetail
    case showRating
    case showReview
    case showAllReviews
    case restaurantList
    case locationList
    case showPhotoReview
    case showPhotoFilter
}

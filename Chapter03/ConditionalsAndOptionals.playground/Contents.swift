import UIKit

// if statements
let isPictureVisible = true
if isPictureVisible {
     print("Picture is visible")
}

let isRestaurantFound = false
if isRestaurantFound == false {
     print("Restaurant was not found")
}

let drinkingAgeLimit = 21
var customerAge = 23
if customerAge < drinkingAgeLimit {
     print("Under age limit")
} else {
     print("Over age limit")
}

// switch statements
var trafficLight = "Yellow"
if trafficLight == "Red" {
     print("Stop")
} else if trafficLight == "Yellow" {
     print("Caution")
} else if trafficLight == "Green" {
     print("Go")
} else {
     print("Invalid Color")
}

// same as above if statement with switch
trafficLight = "Yellow"
switch trafficLight {
     case "Red":
          print("Stop")
     case "Yellow":
          print("Caution")
     case "Green":
          print("Go")
     default:
          print("Invalid color")
}

// optionals and optional binding
/* the print() line will not compile because 'sposeName' variable is not initialised
var spouseName: String
print(spouseName)
*/

var spouseName: String? // optional type
// next line will compile but will produce a warning
print(spouseName)
spouseName = "Janna"
print(spouseName) // Optional is not unwrapped
// next line will not compile because optional cannot be combined with the String
// let greeting = "Hello, " +  spouseName
let greeting = "Hello, " + spouseName! // force unwrap, will crash app if optional has no value

// A better way of handling missing value is to use optional binding.
// optinal binding when value is present
if let spouse = spouseName {
     let greeting = "Hello, " + spouse
     print(greeting)
}
// optinal binding with missing value
spouseName = nil
print(spouseName)
if let spouse = spouseName {
     let greeting = "Hello, " + spouse
     print(greeting)
}


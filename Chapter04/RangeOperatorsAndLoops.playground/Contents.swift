import UIKit

// ranges
let myRange = 10...20   // inclusive
let myRange2 = 10..<20  // exclusive

// for-in loop
for number in myRange {
     print(number)
}
print()

for number in myRange2 {
     print(number)
}
print()

for number in 0...5 {
     print(number)
}
print()

for number in (0...5).reversed() {
     print(number)
}
print()

// while loop
var y = 0
while y < 50 {
     y += 5
     print("y is \(y)")
}
print()

//repeat-while loop
var x = 0
repeat {
     x += 5
     print("x is \(x)")
} while x < 50

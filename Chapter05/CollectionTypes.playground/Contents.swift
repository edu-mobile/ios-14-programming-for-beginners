import UIKit

// arrays
var shoppingList = ["Eggs", "Milk"]

// number of elements in array
shoppingList.count
shoppingList.isEmpty

// add element to an array
shoppingList.append("Cooking Oil")
shoppingList = shoppingList + ["Oranges"]

shoppingList.insert("Chicken", at: 1)

// access an array element
shoppingList[2]

// new value to a particular index
shoppingList[2] = "Soy Milk"
shoppingList

// remove element from an array
shoppingList.remove(at: 1)
shoppingList

// iterate over an array
for shoppingListItem in shoppingList {
     print(shoppingListItem)
}
print()

for shoppingListItem in shoppingList[1...] {
     print(shoppingListItem)
}
print()

// dictionaries
// create
var contactList = ["Shah" : "+60123456789", "Akhil" : "+0223456789" ]

// number of elements in dictionary
contactList.count
contactList.isEmpty

// add a new element to a dictonary
contactList["Jane"] = "+0229876543"
contactList

// access distonary element
contactList["Shah"]

// assign a new value to an existing key
contactList["Shah"] = "+60126789345"
contactList

// remove element from a dictonary
contactList["Jane"] = nil
contactList

var oldDictValue = contactList.removeValue(forKey: "Akhil")
oldDictValue
contactList

// iterating over dictonary
for (name, contactNumber) in contactList {
     print("\(name) : \(contactNumber)")
}
print()

// sets
// create a set
var movieGenres: Set = ["Horror", "Action", "Romantic Comedy" ]

// number of elements in a set
movieGenres.count
movieGenres.isEmpty

// add a new element to a set
movieGenres.insert("War")
movieGenres

// check whether a set contains an element
movieGenres.contains("War")

// remove an item from a set
var oldSetValue = movieGenres.remove("Action")
oldSetValue
movieGenres

// iterate over a set
for genre in movieGenres {
     print(genre)
}

// explore set operations
let movieGenres2: Set = ["Science Fiction", "War", "Fantasy"]
movieGenres.union(movieGenres2)
movieGenres.intersection(movieGenres2)
movieGenres.subtracting(movieGenres2)
movieGenres.symmetricDifference(movieGenres2)

// explore set membership and equality
let movieGenresSubset: Set = ["Horror", "Romantic Comedy"]
let movieGenresSuperset: Set = ["Horror", "Romantic Comedy",   "War", "Science Fiction", "Fantasy"]
let movieGenresDisjoint: Set = ["Bollywood"]
movieGenres == movieGenres2
movieGenresSubset.isSubset(of: movieGenres)
movieGenresSuperset.isSuperset(of: movieGenres)
movieGenresDisjoint.isDisjoint(with: movieGenres)

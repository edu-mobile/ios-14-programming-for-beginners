//
//  Segue.swift
//  LetsEat
//
//  Created by Eduard Luhtonen on 02.11.21.
//

import Foundation

enum Segue:String {
    case showDetail
    case showRating
    case showReview
    case showAllReviews
    case restaurantList
    case locationList
    case showPhotoReview
    case showPhotoFilter
}

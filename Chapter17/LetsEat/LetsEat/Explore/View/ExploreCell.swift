//
//  ExploreCell.swift
//  LetsEat
//
//  Created by Eduard Luhtonen on 02.11.21.
//

import UIKit

class ExploreCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var imgExplore: UIImageView!
}

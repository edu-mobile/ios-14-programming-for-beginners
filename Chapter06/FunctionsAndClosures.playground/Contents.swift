import UIKit

// functions
func serviceCharge() {
     let mealCost = 50
     let serviceCharge = mealCost / 10
     print("Service charge is \(serviceCharge)")
}
serviceCharge()

// function with parameters
func serviceCharge(mealCost: Int) -> Int {
     return mealCost / 10
}
let serviceChargeAmount = serviceCharge(mealCost: 50)
print(serviceChargeAmount)

func serviceCharge(forMealPrice mealCost: Int) -> Int {
     return mealCost / 10
}
let serviceChargeAmount2 = serviceCharge(forMealPrice: 50)
print(serviceChargeAmount2)

// nested functions
func calculateMonthlyPayments(carPrice: Double, downPayment:  Double, interestRate: Double, paymentTerm: Double) -> Double {
     func loanAmount() -> Double {
          return carPrice - downPayment
     }
     func totalInterest() -> Double {
          return interestRate * paymentTerm
     }
     func noOfMonths() -> Double {
          return paymentTerm * 12
     }
     return ((loanAmount() + ( loanAmount() *
     totalInterest() / 100 )) / noOfMonths())
}
calculateMonthlyPayments(carPrice: 50000, downPayment:   5000, interestRate: 3.5, paymentTerm: 7.0)

// function as return type
func makePi() -> (() -> Double) {
     func generatePi() -> Double {
          return 22.0/7.0
}
     return generatePi
}
let pi = makePi()
print(pi())

// function as parameter
func isThereAMatch(listOfNumbers: [Int], condition: (Int) ->  Bool) -> Bool {
      for item in listOfNumbers {
           if condition(item) {
               return true
           }
      }
      return false
}
func oddNumber(number: Int) -> Bool {
      return (number % 2) > 0
}
var numbersList = [2, 4, 6, 7]
isThereAMatch(listOfNumbers: numbersList, condition:   oddNumber)

// Using a guard statement to exit a function early
func buySomething(itemValueField: String, cardBalance: Int) ->  Int {
      guard let itemValue = Int(itemValueField) else {
           print("error in item value")
           return cardBalance
      }
      let remainingBalance = cardBalance - itemValue
      return remainingBalance
}
print(buySomething(itemValueField: "10", cardBalance: 50))
print(buySomething(itemValueField: "blue", cardBalance: 50))
print()

// closures
var numbersArray = [2, 4, 6, 7]
let myClosure = { (number: Int) -> Int in
      let result = number * number
      return result
}
let mappedNumbers = numbersArray.map(myClosure)

// Simplifying closures
var testNumbers = [2, 4, 6, 7]
let mappedTestNumbers = testNumbers.map({ (number: Int)
           -> Int in
           let result = number * number
           return result
})
print(mappedTestNumbers)

// can remove the parameter type, return type, or both
let mappedTestNumbers2 = testNumbers.map({
     number in number * number })
print(mappedTestNumbers2)

// can omit the parentheses
let mappedTestNumbers3 = testNumbers.map {
     number in number * number }
print(mappedTestNumbers3)

// refer to parameters by number instead of by name
let mappedTestNumbers4 = testNumbers.map { $0 * $0 }
print(mappedTestNumbers4)
